$(document).ready(function(){

    var appLoad = {};

    //Nivo Slider :: Home
	$('#idHomeSlideShow').nivoSlider({
	    effect: 'fade',
	    animSpeed: 500,                 
	    pauseTime: 5000,
	    directionNav: false,
	    controlNav: true
	});

	//Carrousel :: Home
	$('.is-featured-tours').owlCarousel({
		loop: true,
		margin: 20,
		nav: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:4
			}
		}
	});

	//Carrousel :: Home/Reviews 
	$('.is-owl-reviews').owlCarousel({
		loop: true,
		margin: 30,
		nav: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});


	//Wowjs
	new WOW().init();

});