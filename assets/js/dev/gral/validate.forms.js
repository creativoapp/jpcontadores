$(document).ready(function(){
    $('a#btnFrmContact').on('click', function(e){
        e.preventDefault();
        $('#frmContact').submit();
    });
    $('#frmContact').on('submit', function(){
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if ($('#inpName').val().trim() == '') {
            $('#inpName').focus();
            return false;
        } else if ($('#inpEmail').val().trim() == '' || regex.test($('#inpEmail').val().trim()) === false) {
            $('#inpEmail').focus();
            return false;
        } else if ($('#inpNumberPhone').val().trim() == '' || $('#inpNumberPhone').val().trim().length < 10) {
            $('#inpNumberPhone').focus();
            return false;
        } else if ($('#inpInteresting').val().trim() == 'any') {
            $('#inpInteresting').focus();
            return false;
        } else if ($('#inpComments').length == 0 || $('#inpComments').val().trim() == '') {
            $('#inpComments').focus();
            return false;
        } else {
            return true;
        }
    });

    //Home
    $('form#inpLightForm').on('submit', function(e){
        //e.preventDefault();
        if ($('#inpName').val().trim() == '') {
            $('#inpName').focus();
            return false;
        } else if ($('#inpNumber').val().trim() == '' || $('#inpNumber').val().trim().length < 10) {
            $('#inpNumber').focus();
            return false;
        } else {
            alert('Gracias por contactar con nosotros!');
            return true;
        }
    });

    $('.number-only').on('input', function() {
        var replace = $(this).val().toLowerCase();
        replace = replace.replace(/[^0-9]/g, '');
        $(this).val(replace);
    });
});