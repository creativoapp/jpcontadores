<?php
class Seo_model
{

    private $tags = null;

    public function metas($page, $lang = 'es')
    {

        $metas = array('title' => 'Asesoría y atención legal en México', 'description' => 'Rubén Frías es un Bufete Juridico que brinda asesoría y atención legal en Cancún, Playa del Carmen, Ciudad de México y Morelos.');

        $tags = json_decode(file_get_contents('models/metas.json'));
        $uri = explode('.', $page);

        if (isset($tags->{'' . $uri[0] . ''}->title)) {
            $metas['title'] = $tags->{'' . $uri[0] . ''}->title;
            $metas['description'] = $tags->{'' . $uri[0] . ''}->description;
        }

        return $metas;
    }
}
