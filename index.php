<?php
include('components/header.php');
?>

<section class="is-home-section">
    <div class="is-slider">
        <div id="idHomeSlideShow" class="nivoSlider">
            <img src="<?= _IMG . 'jp-contadores-cancun-slider-1.jpg'; ?>">
            <img src="<?= _IMG . 'jp-contadores-cancun-slider-2.jpg'; ?>">
        </div>
    </div>



    <div class="container is-services">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h2>Nuestros <br>Servicios</h2>
                <p class="is-big-pr">Ofrecemos servicios contables integrales, no solo cálculo de impuestos o declaraciones anuales, es un servicio profesional que te brindará las herramientas necesarias para que tu empresa pueda obtener estados financieros saludables.</p>
            </div>

            <div class="column is-one-quarter is-item">
                <div>
                    <a href="contabilidad-cancun" class="is-title"><i class="fas fa-calculator"></i> Contabilidad</a>
                    <p>Revisamos status, brindamos consultoría en materia contable, capturamos y actualizamos tu información.</p>
                    <a href="contabilidad-cancun" class="is-link">Saber más</a>
                </div>
            </div>

            <div class="column is-one-quarter is-item">
                <div>
                    <a href="facturacion-cancun" class="is-title"><i class="fas fa-print"></i> Facturación</a>
                    <p>Facturamos tus ingresos, notas de crédito y complemento de pago.</p>
                    <a href="facturacion-cancun" class="is-link">Saber más</a>
                </div>
            </div>

            <div class="column is-one-quarter is-item">
                <div>
                    <a href="nominas-cancun" class="is-title"><i class="fas fa-credit-card"></i> Nominas</a>
                    <p>Procesamiento de la nómina, calculando impuestos y contribuciones de seguridad social tanto de la empresa como de los trabajadores.</p>
                    <a href="nominas-cancun" class="is-link">Saber más</a>
                </div>
            </div>

            <div class="column is-one-quarter is-item">
                <div>
                    <a href="impuestos" class="is-title"><i class="fas fa-satellite"></i> Impuestos</a>
                    <p>Consultoría en materia de impuestos tanto federales como locales y asesoría para planificación fiscal.</p>
                    <a href="impuestos" class="is-link">Saber más</a>
                </div>
            </div>

        </div>
    </div>

    <div class="container description-home">
        <div class="columns">

            <div class="column is-two-thirds">
                <h1>JP Contadores</h1>
                <p class="is-big-pr">JP Contadores surge para dar una atención personalizada al cliente, estar con ellos en la orientación de sus dudas, brindándoles todo el conocimiento y experiencia que tenemos.</p>
            </div>

            <div class="column is-one-third">
                <img src="<?= _IMG . 'cal-il.png'; ?>">
            </div>

        </div>
    </div>

    <div class="is-reasons">
        <div class="is-mask"></div>
        <div class="container">
            <div class="columns">

                <div class="column is-half content-reasons">
                    <h3>¿Por qué elegir JP Contadores en Cancún?</h3>
                    <p class="is-descr is-medium-pr">Somos completamente capaces de cumplir con cada una de tus expectativas asesorándote en los servicios contables que tu empresa o tu necesitan.</p>

                    <div class="columns is-multiline">
                        <div class="column is-half is-item">
                            <i class="fas fa-cog"></i>
                            <strong>Mejora Procesos</strong>
                            <p>Haremos mejorar tus procesos administrativos y contables.</p>
                        </div>
                        <div class="column is-half is-item">
                            <i class="fas fa-headset"></i>
                            <strong>Gestión Profesional</strong>
                            <p>Gestión profesional para brindarles estrategias superiores y cumplimento a tus obligaciones.</p>
                        </div>
                        <div class="column is-half is-item">
                            <i class="fas fa-cubes"></i>
                            <strong>Flexibilidad</strong>
                            <p>Nos adecuamos a las necesidades de tu organización e indicar los riesgos que implican tus operaciones en función de tu giro.</p>
                        </div>
                        <div class="column is-half is-item">
                            <i class="fas fa-shield-alt"></i>
                            <strong>Seguridad</strong>
                            <p>Estrategias para blindar tus operaciones y así consolidar el crecimiento y rentabilidad de tu Negocio</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <img src="<?= _IMG . 'elige-contadores-jp.jpg'; ?>">
    </div>

</section>

<?php include('components/footer.php'); ?>