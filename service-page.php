<?php include('components/header.php'); ?>

<section class="is-page is-service-section">
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1>Titulo del Servicio</h1>
                <p class="is-medium-pr">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen.</p>
                <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
            
                <strong>Información en lista</strong>
                <ul>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                    <li><i class="far fa-check-circle"></i>Información item</li>
                </ul>
            </div>

            <div class="column is-half is-faqs">
                <div class="is-question">
                    <strong><span>1.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>2.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>3.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>4.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>5.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
            </div>

        </div>
    </div>


    <div class="is-services">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h2>Otros servicios</h2>
                </div>

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-calculator"></i> Contabilidad</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>  

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-print"></i> Facturación</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>  

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-credit-card"></i> Nominas</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>  

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-satellite"></i> Impuestos</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-calculator"></i> Contabilidad</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>  

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-print"></i> Facturación</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>  

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-credit-card"></i> Nominas</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>  

                <div class="column is-one-quarter is-item">
                    <div>
                        <a href="#" class="is-title"><i class="fas fa-satellite"></i> Impuestos</a>
                        <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                        <a href="#" class="is-link">Saber más</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php include('components/footer.php'); ?>