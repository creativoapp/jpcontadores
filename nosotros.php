<?php include('components/header.php'); ?>

<section class="is-page is-about-section">
    <div class="container">
        <div class="columns">
            <div class="column is-full is-overview">
                <h1>JP Contadores</h1>
               
                <p class="is-big-pr">Somos un despacho joven, con buenas aptitudes, estando a la par en la evolución y crecimiento, contamos con un equipo de alto nivel profesional que sabe dar respuesta a las necesidades de nuestros clientes.</p>

                <p class="is-medium-pr">Tendrán siempre la confianza en la generación de sus Estados Financieros y reporte de cada movimiento mes a mes para el análisis y cumplimiento oportuno en temas fiscales.</p>

                <p>Le daremos la seguridad que usted requiere y una atención personal y que le haremos comprender todas sus dudas en un lenguaje sencillo y un servicio de calidad, eficaz, oportuna y completa.</p>

            </div>
        </div>
    </div>

    <div class="container is-reasons">
        <div class="columns">

            <div class="column is-one-quarter is-item">
                <div>
                    <strong><span>1</span>Mejora Procesos</strong>
                    <p>Haremos mejorar tus procesos administrativos y contables.</p>
                </div>
            </div>

            <div class="column is-one-quarter is-item">
                <div>
                    <strong><span>2</span>Gestión Profesional</strong>
                    <p>Gestión profesional para brindarles estrategias superiores y cumplimento a tus obligaciones.</p>
                </div>
            </div>

            <div class="column is-one-quarter is-item">
                <div>
                    <strong><span>3</span>Flexibilidad</strong>
                    <p>Nos adecuamos a las necesidades de tu organización e indicar los riesgos que implican tus operaciones en función de tu giro.</p>
                </div>
            </div>

            <div class="column is-one-quarter is-item">
                <div>
                    <strong><span>4</span>Seguridad</strong>
                    <p>Estrategias para blindar tus operaciones y así consolidar el crecimiento y rentabilidad de tu Negocio</p>
                </div>
            </div>

        </div>
    </div>

    <div class="is-organization">
        <div class="is-mask"></div>
        <div class="container">
            <div class="columns is-multiline">
            
                <div class="column is-full">
                    <h3>Tenemos claro el camino</h3>
                </div>

                <div class="column is-half is-box">
                    <h2><i class="far fa-eye"></i>Nuestra Visión</h2>
                    <p>Somos el apoyo idóneo de las decisiones de nuestros clientes gracias a la confiabilidad y calidad de nuestros profesionales y así generando cada día mas la rentabilidad de nuestro despacho</p>
                </div>

                <div class="column is-half is-box">
                    <h2><i class="fas fa-bullseye"></i>Nuestra Misión</h2>
                    <p>Satisfacer las necesidades de nuestros clientes, brindando un servicio personalizado y ser su asesor estratégico y así dejando sus problemas contables y fiscales en manos de los especialistas.</p>
                </div>

                <div class="column is-full is-box">
                    <h3>Valores JP</h3>
                    <ul>
                        <li>Atrevimiento</li>
                        <li>Responsabilidad</li>
                        <li>Puntualidad</li>
                        <li>Confidencialidad</li>
                    </ul>
                </div>

            </div>
        </div>
    </div>

    <!--<div class="is-team">
        <div class="container">

            <h3>Nuestros Expertos</h3>

            <div class="columns">

                <div class="column is-one-quarter">
                    <img src="<?=_IMG.'team-01.jpg';?>">
                    <strong>Lic. Juan Piña</strong>
                    <span>Director General</span>
                </div>

                <div class="column is-one-quarter">
                    <img src="<?=_IMG.'team-02.jpg';?>">
                    <strong>Lic. Kristell Castillo</strong>
                    <span>Contador Fiscal</span>
                </div>

                <div class="column is-one-quarter">
                    <img src="<?=_IMG.'team-03.jpg';?>">
                    <strong>Lic. Juan Piña</strong>
                    <span>Contador Financiero</span>
                </div>

                <div class="column is-one-quarter">
                    <img src="<?=_IMG.'team-04.jpg';?>">
                    <strong>Lic. Kristell Castillo</strong>
                    <span>Impuesto / Seguro Social</span>
                </div>

            </div>
        </div>
    </div>-->

</section>

<?php include('components/footer.php'); ?>