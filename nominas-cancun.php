<?php include('components/header.php'); ?>

<section class="is-page is-service-section">
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1>Nominas</h1>
                <p class="is-medium-pr">Procesamiento de la nómina, calculando impuestos y contribuciones de seguridad social tanto de la empresa como de los trabajadores.</p>
                <p>Se imprime los recibos de nómina de cada trabajador.</p>

                <strong>Información</strong>
                <ul>
                    <li><i class="far fa-check-circle"></i>Movimiento y gestoría antes el IMSS</li>
                    <li><i class="far fa-check-circle"></i>Nomina empresarial</li>
                    <li><i class="far fa-check-circle"></i> de la nomina</li>
                    <li><i class="far fa-check-circle"></i> del Impuesto sobre Nomina</li>
                </ul>
            </div>

            <div class="column is-half is-faqs">
                <div class="is-question">
                    <strong><span>1.</span> ¿QUE NECESITO PARA EMITIR UN RECIBO DE NOMINA A MIS TRABAJADORES?</strong>
                    <p>
                        <ol type="A" style="margin-left: 40px;">
                            <li>RFC TRABAJADORES</li>
                            <li>NUMERO DE SEGURO SOCIAL DE TRABAJADORES</li>
                            <li>IDENTIFICACION</li>
                            <li>SALARIO DIARIO</li>
                            <li>ESTAR INSCRITO Y CONTAR CON TU REGISTRO PATRONAL</li>
                        </ol>
                    </p>
                </div>
                <div class="is-question">
                    <strong><span>2.</span> ¿QUE ES EL REGISTRO PATRONAL?</strong>
                    <p>Es un dato proporcionado por el imss al momento de afiliarte como patron.</p>
                </div>
            </div>

        </div>
    </div>

    <div class="is-services">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h2>Otros servicios</h2>
                </div>

                <?php 
                $serviceCurrent = 'NS';
                include('components/services-list.php');
                ?>

            </div>
        </div>
    </div>
</section>

<?php include('components/footer.php'); ?>