<?php include('components/header.php'); ?>

<section class="is-page is-service-section">
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1>Tramites</h1>

                <strong>Información</strong>
                <ul>
                    <li><i class="far fa-check-circle"></i>Uso de suelo</li>
                    <li><i class="far fa-check-circle"></i>Apertura de negocio</li>
                    <li><i class="far fa-check-circle"></i>Notariales</li>
                    <li><i class="far fa-check-circle"></i>SAT, firma electrónica, clave CIEC</li>
                </ul>
            </div>

            <div class="column is-half is-faqs">
                <!--<div class="is-question">
                    <strong><span>1.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>2.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>3.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>4.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>
                <div class="is-question">
                    <strong><span>5.</span> ¿Pregunta relacionada al tipo de servicio?</strong>
                    <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>
                </div>-->
            </div>

        </div>
    </div>

    <div class="is-services">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h2>Otros servicios</h2>
                </div>

                <?php 
                $serviceCurrent = 'TA';
                include('components/services-list.php');
                ?>

            </div>
        </div>
    </div>
</section>

<?php include('components/footer.php'); ?>