module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		cssmin: {
		  	target: {
		    	files: [{
					expand: true,
					cwd: 'assets/css/dev/site/',
					src: ['*.css', '*.min.css'],
					dest: 'assets/css/',
					ext: '.min.css',
					cwd: 'assets/css/dev/components/',
					src: ['*.css', '*.min.css'],
					dest: 'assets/css/dev/components/min',
					ext: '.min.css'
				}]
			}
		},

		stylus: {
		  	compile: {
				files: {
					'assets/css/dev/site/jp.css' : 'assets/css/dev/jp.styl',
					'assets/css/jp.min.css' : 'assets/css/dev/site/jp.css'
				}
			}
		},

		concat: {
		    dist: {
		      	src: ['assets/css/dev/components/min/*.min.css'],
		      	dest: 'assets/css/components.min.css',
			}
		},

		uglify : {
			my_target: {
		     	files: {
					'assets/js/jp.js': ['assets/js/dev/gral/*.js'],
					'assets/js/components.js': ['assets/js/dev/components/*.js']
		      	}
		    }
		},

		//Watch changes
		watch: {
			css: {
				files: ['assets/css/dev/*.styl', 'assets/css/dev/components/*.css', 'assets/js/dev/gral/*.js'],
				tasks: ['stylus', 'cssmin', 'concat', 'uglify']
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['stylus', 'cssmin', 'concat', 'uglify']);

}
