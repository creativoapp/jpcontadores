<?php include('components/header.php'); ?>

<section class="is-page is-service-section">
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1>Facturación</h1>
                <p class="is-medium-pr">Facturamos tus ingresos, notas de crédito y complemento de pago.</p>

                <strong>Información</strong>
                <ul>
                    <li><i class="far fa-check-circle"></i>Capacitación para realizar sus propias facturas.</li>
                </ul>
            </div>

            <div class="column is-half is-faqs">
                <div class="is-question">
                    <strong><span>1.</span> ¿QUE ES UNA FACTURA?</strong>
                    <p>Es una relacion de los objetos o articulos comprendidos en una venta o servicio.</p>
                </div>
                <div class="is-question">
                    <strong><span>2.</span> ¿QUE ES UN COMPLEMENTO DE PAGO Y CUANDO DE EXPIDE?</strong>
                    <p>Es un XML adjunto al CFDI (factura), se expide por la recepcion de pago ya sea en parcialidades o una sola exhibicion.</p>
                </div>
                <div class="is-question">
                    <strong><span>3.</span> ¿SE DEBE EMITIR UNA FACTURA GLOBAL POR VENTAS A PUBLICO GENERAL?</strong>
                    <p>Cuando los contribuyentes expidan comprobantes con operaciones con el publico general, pueden expedir una factura electronica diaria, semanal o mensual, utilizando para ello la clave generica: XAXX010101000</p>
                </div>
            </div>

        </div>
    </div>

    <div class="is-services">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h2>Otros servicios</h2>
                </div>

                <?php 
                $serviceCurrent = 'FT';
                include('components/services-list.php');
                ?>

            </div>
        </div>
    </div>
</section>

<?php include('components/footer.php'); ?>