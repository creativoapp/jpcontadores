<?php include('components/header.php'); ?>

<section class="is-page is-service-section">
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1>Contabilidad</h1>
                <p class="is-medium-pr">Revisamos status, brindamos consultoria en materia contable, capturamos y actualizamos tu información.</p>

                <strong>Información</strong>
                <ul>
                    <li><i class="far fa-check-circle"></i>Regularización de ejercicio anteriores</li>
                    <li><i class="far fa-check-circle"></i>Consultoria y capacitación</li>
                    <li><i class="far fa-check-circle"></i>Analisis y diagnostico de situación financiera</li>
                    <li><i class="far fa-check-circle"></i>Contabilidad electrónica</li>
                </ul>
            </div>

            <div class="column is-half is-faqs">
                <div class="is-question">
                    <strong><span>1.</span> ¿QUE ES UN GASTO DEDUCIBLE?</strong>
                    <p>Cualquier gasto que tenga algo que ver con aquello a lo que se dedica tu empresa y que tiene que ver con tu actividad diaria.</p>
                </div>
                <div class="is-question">
                    <strong><span>2.</span> ¿TIPOS DE GASTO NO DEDUCIBLES?</strong>
                    <p>Sanciones o multas, donativos y regalos.</p>
                </div>
                <div class="is-question">
                    <strong><span>3.</span> ¿QUÉ ES LA CONTABILIDAD ELECTRONICA?</strong>
                    <p>Es el envio de informacion por medios digitales de todas las transacciones realizadas en el mes.</p>
                </div>
            </div>

        </div>
    </div>

    <div class="is-services">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h2>Otros servicios</h2>
                </div>

                <?php 
                $serviceCurrent = 'CT';
                include('components/services-list.php');
                ?>

            </div>
        </div>
    </div>
</section>

<?php include('components/footer.php'); ?>