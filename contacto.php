<?php include('components/header.php'); ?>

<section class="is-contact-section">
    <div class="is-contact">
        <div class="is-mask"></div>
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h1>Contáctanos</h1>
                    <p class="is-big-pr">Para JP Contadores en muy importante brindarte la atención oportuna a tiempo, contactános por cualquiera de los siguientes medios.</p>
                </div>

                <div class="column is-one-quarter is-item">
                    <i class="fas fa-phone" aria-hidden="true"></i>
                    <strong>Llámanos</strong>
                    <p>(998) 264 2916</p>
                </div>

                <div class="column is-one-quarter is-item">
                    <i class="fas fa-envelope-open-text" aria-hidden="true"></i>
                    <strong>Escríbenos</strong>
                    <p>info@jpcontadorescancun.com</p>
                </div>

                <div class="column is-one-quarter is-item">
                    <i class="fab fa-facebook-f"></i>
                    <strong>Síguenos</strong>
                    <a href="https://www.facebook.com/jp.contadores" target="_blank" class="is-facebook">Facebook</a>
                    <a href="#" target="_blank" class="is-instagram">Instagram</a>
                </div>

                <div class="column is-one-quarter is-item">
                    <i class="fas fa-map-marker-alt" aria-hidden="true"></i>
                    <strong>Localizanos</strong>
                    <p>Cancún, Quintana Roo</p>
                </div>

            </div>
        </div>
    </div>

    <div class="container">
        <div class="columns">
        
            <div class="column is-half">
                <form name="frmContact" id="frmContact" class="columns is-multiline" action="/hola" method="POST">
                    <fieldset class="column is-full">
                        <label>Nombre</label>
                        <input type="text" name="inpName" id="inpName">
                    </fieldset>
                    <fieldset class="column is-two-thirds">
                        <label>Correo Electrónico</label>
                        <input type="text" name="inpEmail" id="inpEmail">
                    </fieldset>
                    <fieldset class="column is-one-third">
                        <label>Teléfono</label>
                        <input type="text" class="number-only" name="inpNumberPhone" id="inpNumberPhone">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label>¿Estas Interesado en?</label>
                        <select name="inpInteresting" id="inpInteresting">
                            <option value="any">Elija un servicio</option>
                            <option value="Contabilidad">Contabilidad</option>
                            <option value="Facturación">Facturación</option>
                            <option value="Nominas">Nominas</option>
                            <option value="Impuestos">Impuestos</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-half">
                        <label>¿Cómo se entero de nosotros?</label>
                        <select name="inpChannel" id="inpChannel">
                            <option value="Google">Google</option>
                            <option value="Redes Sociales">Redes Sociales</option>
                            <option value="Amigo">Un amigo</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-full">
                        <label>Cuéntanos, queremos ayudarte.</label>
                        <textarea name="inpComments" id="inpComments" rows="5"></textarea>
                    </fieldset>
                    <fieldset class="column is-one-quarter">
                        <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
                        <input type="submit" name="btnFrmContact" id="btnFrmContact" value="CONTACTAR">
                    </fieldset>
                </form>
            </div>

        </div>
    </div>

</section>

<?php include('components/footer.php'); ?>