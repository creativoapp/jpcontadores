<form name="frmServiceDetail" id="frmServiceDetail" action="/hola" method="post" class="columns is-multiline wow slideInRight" data-wow-duration="1s" data-wow-delay=".7s">
    <fieldset class="column is-full">
        <label>Nombre completo</label>
        <input type="text" name="inpName" id="inpName">
    </fieldset>
    <fieldset class="column is-two-thirds">
        <label>Email</label>
        <input type="text" name="inpEmail" id="inpEmail">
    </fieldset>
    <fieldset class="column is-one-third">
        <label>Teléfono</label>
        <input type="text" name="inpNumberPhone" id="inpNumberPhone">
    </fieldset>
    <fieldset class="column is-full">
        <input type="hidden" name="inpServiceInteresting" id="inpServiceInteresting" value="<?=$serviceInteresting;?>">
    </fieldset>
    <fieldset class="column is-half">
        <label>Asunto</label>
        <input type="text" name="inpSubject" id="inpSubject">
    </fieldset>
    <fieldset class="column is-half">
        <label>¿Dónde estas?</label>
        <select name="inpWhere" id="inpWhere">  
            <option value="Cancun/Playa">Cancún / Playa del Carmen</option>
            <option value="CMexico">Ciudad de México</option>
            <option value="Morelos">Morelos</option>
        </select>
    </fieldset>
    <fieldset class="column is-full">
        <label>Comentarios</label>
        <textarea name="inpComments" id="inpComments" rows="5"></textarea>
    </fieldset>
    <fieldset class="column is-full">
        <a id="submit-contact" href="#"><i class="fas fa-fax"></i> Contactar</a>
        <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
    </fieldset>
</form>