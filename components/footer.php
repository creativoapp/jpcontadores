
    <footer class="is-footer">
        <div class="container">
            <div class="columns">

                <div class="column is-full">
                    <a href="/"><img src="<?= _IMG . 'jp-contadores-white.png';?>"></a>
                    <div class="is-social">
                        <a href="https://www.facebook.com/jp.contadores" target="_blank" class="is-facebook"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" target="_blank" class="is-instagram"><i class="fab fa-instagram"></i></a>
                    </div>

                    <ul>
                        <li><a href="/nosotros">JP Contadores</a></li>
                        <li><a href="#">Servicio</a></li>
                        <li><a href="#">Servicio</a></li>
                        <li><a href="#">Servicio</a></li>
                        <li><a href="#">Servicio</a></li>
                        <li><a href="/contacto">Contacto</a></li>
                        <li><a href="#">Blog</a></li>
                    </ul>

                    <small class="is-by">© <?=date('Y');?> JP Contadores. Todos los derechos reservados.</small>
                </div>

            </div>
        </div>
    </footer>


    <script type="text/javascript" src="/assets/js/jquery-1.9.0.min.js"></script>
    <script type="text/javascript" src="/assets/js/components.js?v=<?=$cacheVr;?>"></script>
	<script type="text/javascript" src="/assets/js/jp.js?v=<?=$cacheVr;?>"></script>

</body>
</html>