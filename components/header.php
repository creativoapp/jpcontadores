<!DOCTYPE html>
<html lang="en">
<head>

    <?php 
        $cacheVr = 4.1; 
        define('_IMG', '/assets/img/');
        $page = basename($_SERVER['SCRIPT_NAME']);
    ?>

    <meta charset="UTF-8">
    
    <?php
        include ('models/seo.php');
        $seo = new Seo_model();
        $metas = (object)$seo->metas($page);
    ?>
    <title><?= $metas->title; ?> | JP Contadores</title>
    <meta name="description" content="<?= $metas->description; ?>"/>


	<meta name="viewport" content="width=device-width, user-scalable=no" />
	<link rel="shortcut icon" type="image/png" href="<?=_IMG.'favicon.ico';?>"/>
	
	<!-- STYLES -->
	<link rel="stylesheet" href="/assets/css/bulma.min.css?v=<?=$cacheVr;?>">
    <link rel="stylesheet" href="/assets/css/components.min.css?v=<?=$cacheVr;?>">
    <link rel="stylesheet" href="/assets/css/jp.min.css?v=<?=$cacheVr;?>">

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

	<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- CAPTCHA -->
    <?php if ($_SERVER['PHP_SELF'] == '/contacto.php') { ?>
        <script src="https://www.google.com/recaptcha/api.js?render=6Ld1V_kUAAAAAJ2PdxFR4cDEqF6ungl7V4-4k0T5"></script>
        <script type="text/javascript">
            grecaptcha.ready(function() {
                grecaptcha.execute('6Ld1V_kUAAAAAJ2PdxFR4cDEqF6ungl7V4-4k0T5', {
                        action: 'homepage'
                    })
                    .then(function(token) {
                        document.getElementById('g-recaptcha-response').value = token;
                    });
            });
            setInterval("newCaptcha()", 50000);
            function newCaptcha(){
                grecaptcha.execute('6Ld1V_kUAAAAAJ2PdxFR4cDEqF6ungl7V4-4k0T5', {action: 'homepage'}).then(function(token){
                    document.getElementById('g-recaptcha-response').value=token;
                });
            }
        </script>
    <?php } ?>

</head>
<body class="is-body">

    <header class="is-header">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-one-quarter is-brand">
                    <a href="/"><img src="<?=_IMG.'jp-contadores.jpg';?>"></a>
                </div>

                <div class="column is-one-quarter is-box-contact is-mail">
                    <span>Escríbenos</span>
                    <a href="mailto:info@jpcontadorescancun.com">info@jpcontadorescancun.com</a>
                    <i class="fas fa-envelope-open-text"></i>
                </div>
    
                <div class="column is-one-quarter is-box-contact">
                    <span>Llámanos</span>
                    <a href="tel:9982642916">(998) 264 2916</a>
                    <i class="fas fa-phone"></i>
                </div>
    
                <div class="column is-one-quarter is-box-contact">
                    <span>Dirección</span>
                    <a href="#">Cancún, Quintana Roo</a>
                    <i class="fas fa-map-marker-alt"></i>
                </div>

                <a id="btn-menu" href="#"><i class="fa fa-bars"></i></a>

            </div>

            <div class="columns">
                <div class="column is-full is-box-nav">
                    <ul class="is-menulist">
                        <li><a href="/" <?php if($page == 'index.php') { ?> class="is-current" <?php } ?>>Inicio</a></li>
                        <li><a href="/nosotros" <?php if($page == 'nosotros.php') { ?> class="is-current" <?php } ?>>Nosotros</a></li>
                        <li><a href="#">Servicios</a>
                            <ul class="is-level">
                                <li><a href="/contabilidad-cancun" class="is-title"><i class="fas fa-calculator"></i> Contabilidad</a></li>
                                <li><a href="/facturacion-cancun" class="is-title"><i class="fas fa-print"></i> Facturación</a></li>
                                <li><a href="/nominas-cancun" class="is-title"><i class="fas fa-credit-card"></i> Nominas</a></li>
                                <li><a href="/impuestos" class="is-title"><i class="fas fa-satellite"></i> Impuestos</a></li>
                                <li><a href="/tramites-cancun" class="is-title"><i class="fas fa-file-alt"></i> Tramites</a></li>
                            </ul>
                        </li>  
                        <li><a href="/contacto" <?php if($page == 'contacto.php') { ?> class="is-current" <?php } ?>>Contacto</a></li>                  
                        <li><a href="/blog/">Blog</a></li>
                    </ul>
                    <a href="/contacto" class="is-quote">Pide una asesoría</a>
                </div>
            </div>
        </div>
    </header>
