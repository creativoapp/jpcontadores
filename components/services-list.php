<?php if($serviceCurrent !=  'CT') { ?>
<div class="column is-one-quarter is-item">
    <div>
        <a href="contabilidad-cancun" class="is-title"><i class="fas fa-calculator"></i> Contabilidad</a>
        <p>Revisamos status, brindamos consultoria en materia contable, capturamos y actualizamos tu información.</p>
        <a href="contabilidad-cancun" class="is-link">Saber más</a>
    </div>
</div>
<?php } ?>

<?php if($serviceCurrent !=  'FT') { ?>
<div class="column is-one-quarter is-item">
    <div>
        <a href="facturacion-cancun" class="is-title"><i class="fas fa-print"></i> Facturación</a>
        <p>Te asesoramos y ayudamos con la facturación tus ingresos, notas de crédito y complementos de pago.</p>
        <a href="facturacion-cancun" class="is-link">Saber más</a>
    </div>
</div>
<?php } ?>

<?php if($serviceCurrent !=  'NS') { ?>
<div class="column is-one-quarter is-item">
    <div>
        <a href="nominas-cancun" class="is-title"><i class="fas fa-credit-card"></i> Nominas</a>
        <p>Procesamiento tu nómina, calculando impuestos y contribuciones de seguridad social tanto de la empresa como de los trabajadores.</p>
        <a href="nominas-cancun" class="is-link">Saber más</a>
    </div>
</div>
<?php } ?>

<?php if($serviceCurrent !=  'IS') { ?>
<div class="column is-one-quarter is-item">
    <div>
        <a href="impuestos" class="is-title"><i class="fas fa-satellite"></i> Impuestos</a>
        <p>Consultoría en materia de impuestos tanto federales como locales y asesoría para planificación fiscal.</p>
        <a href="impuestos" class="is-link">Saber más</a>
    </div>
</div>
<?php } ?>

<?php if($serviceCurrent !=  'TA') { ?>
<div class="column is-one-quarter is-item">
    <div>
        <a href="tramites-cancun" class="is-title"><i class="fas fa-calculator"></i> Trámites</a>
        <p>Te asesoramos y auxiliamos en el proceso de trámites contables, notariales, municipales y más.</p>
        <a href="tramites-cancun" class="is-link">Saber más</a>
    </div>
</div>
<?php } ?>