<?php include('components/header.php'); ?>

<section class="is-page is-service-section">
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h1>Impuestos</h1>
                <p class="is-medium-pr">Consultoria en materia de impuestos tanto federales como locales y asesoría para planificación fiscal.</p>

                <strong>Información</strong>
                <ul>
                    <li><i class="far fa-check-circle"></i> fiscal y de negocios</li>
                    <li><i class="far fa-check-circle"></i>Fiscalizacion y presentación de los Impuestos</li>
                    <li><i class="far fa-check-circle"></i>Elaboracion de Estados Financieros</li>
                    <li><i class="far fa-check-circle"></i>Calculo de impuestos anuales</li>
                </ul>
            </div>

            <div class="column is-half is-faqs">
                <div class="is-question">
                    <strong><span>1.</span> ¿QUE SON LOS IMPUESTOS?</strong>
                    <p>Es un tributo que las personas estan obligadas a pagar a alguna organización (GOBIERNO, SAT, IMSS)</p>
                </div>
                <div class="is-question">
                    <strong><span>2.</span> ¿POR QUE Y PARA QUE SE PAGAN IMPUESTOS?</strong>
                    <p>Es necesario para sostener los gastos publicos, buscar una mayor eficiencia de la economia.</p>
                </div>
                <div class="is-question">
                    <strong><span>3.</span> ¿CUALES SON LOS TIPOS DE IMPUESTOS?</strong>
                    <p>
                        <ol type="A" style="margin-left: 40px;">
                            <li>IMPUESTO SOBRE LA RENTA (ISR)</li>
                            <li>IMPUESTO AL VALOR AGREGADO (IVA)</li>
                            <li>IMPUESTO ESPECIAL SOBRE PRODUCCION Y SERVICIOS (IEPS)</li>
                            <li>IMPUESTO SOBRE NOMINA (ISN)</li>
                        </ol>
                    </p>
                </div>
            </div>

        </div>
    </div>

    <div class="is-services">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h2>Otros servicios</h2>
                </div>

                <?php 
                $serviceCurrent = 'IS';
                include('components/services-list.php');
                ?>

            </div>
        </div>
    </div>
</section>

<?php include('components/footer.php'); ?>